import api from '../../boot/axios'

export const NAMESPACE = 'guidebooks'

// Getters
export const STORES = 'guidebooks'
export const STORE = 'guidebook'
export const ISLOADING = 'isLoading'

// Actions
export const GET_GUIDEBOOKS = 'getGuidebooks'
export const GET_GUIDEBOOK = 'getGuidebook'

// Mutations
export const SET_GUIDEBOOKS = 'setGuidebooks'
export const SET_GUIDEBOOK = 'setGuidebook'
export const SET_ISLOADING = 'setIsLoading'

// initial state
const state = {
  guidebooks: [],
  guidebook: {},
  isLoading: false
}

// getters
const getters = {
  [STORES]: (state) => {
    return state.guidebooks
  },
  [STORE]: (state) => {
    return state.guidebook
  },
  [ISLOADING]: (state) => {
    return state.isLoading
  }
}

// actions
const actions = {
  /**
   * [GET_GUIDEBOOKS] - Retrieve Guidebooks
   *
   * @param  {function} commit Function to call mutations
   * @param  {Object}   state  Object with the actual state of the module
   */
  async [GET_GUIDEBOOKS] ({ commit, dispatch, state, getters }, { sortBy, limit, search }) {
    try {
      commit(SET_ISLOADING, true)
      const params = { sortBy, limit, search: JSON.stringify(search) }
      const { data } = await api.get('/guidebooks', { params })
      if (data && data.items && data.items.length > 0) {
        commit(SET_GUIDEBOOKS, data.items)
      }
    } finally {
      commit(SET_ISLOADING, false)
    }
  },
  /**
   * [GET_GUIDEBOOK] - Retrieve Guidebook
   *
   * @param  {function} commit Function to call mutations
   * @param  {Object}   state  Object with the actual state of the module
   */
  async [GET_GUIDEBOOK] ({ commit, dispatch, state, getters }, { guidebookId }) {
    try {
      commit(SET_ISLOADING, true)
      const { data } = await api.get('/guidebook/' + guidebookId)
      commit(SET_GUIDEBOOK, data)
    } finally {
      commit(SET_ISLOADING, false)
    }
  }
}

// mutations
const mutations = {
  [SET_GUIDEBOOKS] (state, stores) {
    state.stores = stores
  },
  [SET_GUIDEBOOK] (state, store) {
    state.store = store
  },
  [SET_ISLOADING] (state, isLoading) {
    state.isLoading = isLoading
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
